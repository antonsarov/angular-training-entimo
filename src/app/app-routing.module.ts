import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IssueListComponent} from './issue-list/issue-list.component';
import {IssueDetailsComponent} from './issue-details/issue-details.component';


const routes: Routes = [
  { path: '', redirectTo: '/issues', pathMatch: 'full' },
  { path: 'issues', component: IssueListComponent },
  { path: 'issues/:id', component: IssueDetailsComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}
