import { Injectable } from '@angular/core';
import {Issue} from './issue';
import {from, Observable, of} from 'rxjs';
import {filter, first} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  issues: Issue[] = [{
    id: 1,
    summary: 'Fix the bug',
    description: 'Fix the bug introduced in the last sprint',
    storyPoints: 10
  }, {
    id: 2,
    summary: 'Implement the feature',
    description: 'Implement the cool feature',
    storyPoints: 15
  }, {
    id: 3,
    summary: 'Change the style',
    description: 'Make it more beautiful',
    storyPoints: 7
  }];

  constructor(private httpClient: HttpClient) { }

  getIssues(): Observable<Issue[]> {
    return this.httpClient.get<Issue[]>('https://fast-earth-48225.herokuapp.com/issues');
  }

  getIssue(id: number): Observable<Issue> {
    return this.httpClient.get<Issue>('https://fast-earth-48225.herokuapp.com/issues/' + id);
  }
}
