import { Component, OnInit } from '@angular/core';
import {Issue} from '../issue';
import {IssueService} from '../issue.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-issue-details',
  templateUrl: './issue-details.component.html',
  styleUrls: ['./issue-details.component.css']
})
export class IssueDetailsComponent implements OnInit {

  issue: Issue;

  constructor(private route: ActivatedRoute, private issueService: IssueService) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.issueService.getIssue(id).subscribe(value => this.issue = value);
  }

}
