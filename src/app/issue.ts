export class Issue {
  id: number;
  summary: string;
  description: string;
  storyPoints: number;
}
